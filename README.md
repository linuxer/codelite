pango needs patches as follows (for the time being, please apply manually):
1) /usr/local/include/pango-1.0/pango/pango-coverage.h ----> L28: #include <harfbuzz/hb.h>
2) /usr/local/include/pango-1.0/pango/pango-font.h ----> L29: #include <harfbuzz/hb.h>

Some Deps:
- llvm90
- gtk3
- cairo
- py38-pygments

build command against llvm12:

- make MAKE_JOBS_UNSAFE=yes install clean
